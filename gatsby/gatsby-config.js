module.exports = {
  pathPrefix: `/catalogs/helmholtz-imaging`,
  siteMetadata: {
    title: 'Image data analysis solutions',
    subtitle: 'Curated by Helmholtz Imaging',
    catalog_url: 'https://gitlab.com/album-app/catalogs/helmholtz-imaging',
    menuLinks:[
      {
         name:'Catalog',
         link:'/catalog'
      },
      {
         name:'About',
         link:'/about'
      },
    ]
  },
  plugins: [{ resolve: `gatsby-theme-album`, options: {} }],
}
