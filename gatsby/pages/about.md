---
layout: page
title: About
permalink: /about/
---

This catalog is a collection of [Album](https://album.solutions) solutions curated by [Helmholtz Imaging](https://helmholtz-imaging.de). 
These solutions were built for past or ongoing collaborations, projects, or support requests at Helmholtz Imaging and wrap existing tools or algorithms for common image data analysis problems. 
Please refer to the citation displayed for each solution in order to cite the authors of the tools used in each solution. Use them at your own risk.